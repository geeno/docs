var path = require('path')

console.log(path.join(__dirname, '/dist/docs/'))

module.exports = {
  title: "Geeno's Docs",
  //description: '',
  //设置网站生成时的路径prefix，根路径
  base: '/docs/',
  //host: '192.168.1.100',
  //dest: path.join(__dirname, '/dist/docs/'),
  port: 65533,
  //extraWatchFiles: ['.vuepress/config.js'],
  evergreen: true,
  //cache: false,
  //配置markdown相关
  markdown: {
    //显示行号
    //lineNumbers: true,
  },
  head: [
    ['link', { rel: 'icon', href: `/logo.png` }],
    ['meta', { name: 'theme-color', content: '#3eaf7c' }],
    ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
    [
      'meta',
      { name: 'apple-mobile-web-app-status-bar-style', content: 'black' },
    ],
    [
      'link',
      { rel: 'apple-touch-icon', href: `/icons/apple-touch-icon-152x152.png` },
    ],
    [
      'link',
      {
        rel: 'mask-icon',
        href: '/icons/safari-pinned-tab.svg',
        color: '#3eaf7c',
      },
    ],
    [
      'meta',
      {
        name: 'msapplication-TileImage',
        content: '/icons/msapplication-icon-144x144.png',
      },
    ],
    ['meta', { name: 'msapplication-TileColor', content: '#000000' }],
  ],
  themeConfig: {
    nav: [
      {
        text: 'Home',
        link: '/',
      },
      {
        text: 'Guide',
        link: '/guide/',
      },
      {
        text: 'FrontEnd',
        items: [
          {
            text: 'Common',
            items: [
              { text: 'Html', link: '/frontend/common/html/' },
              { text: 'Css', link: '/frontend/common/css/' },
              { text: 'Javascript', link: '/frontend/common/javascript/' },
            ],
          },
          {
            text: 'Extra',
            items: [
              {
                text: 'Framework',
                link: '/frontend/extra/framework/',
              },
              {
                text: 'Plugins',
                link: '/frontend/extra/plugins/',
              },
            ],
          },
        ],
      },
      {
        text: 'Bakend',
        items: [
          {
            text: 'Develop',
            items: [
              { text: 'AspNetCore', link: '/bakend/develop/aspnetcore/' },
              { text: 'CsharSpec', link: '/bakend/develop/csharpspec/' },
              { text: 'Linq', link: '/bakend/develop/linq/' },
            ],
          },
          {
            text: 'Database',
            items: [
              { text: 'Mysql', link: '/bakend/database/mysql/' },
              { text: 'Sqlite', link: '/bakend/database/sqlite/' },
              { text: 'Sqlserver', link: '/bakend/database/sqlserver/' },
            ],
          },
          {
            text: 'Plugins',
            items: [{ text: 'Nlog', link: '/bakend/plugins/nlog/' }],
          },
        ],
      },
      {
        text: 'Windows',
        items: [
          {
            text: 'Tools',
            items: [
              {
                text: 'Word',
                link: '/windows/tools/word/',
              },
              {
                text: 'Media',
                link: '/windows/tools/media/',
              },
              {
                text: 'Wallpaper',
                link: '/windows/tools/wallpaper/',
              },
              {
                text: 'FireWall',
                link: '/windows/tools/firewall/',
              },
            ],
          },
          {
            text: 'CommandLine',
            items: [
              {
                text: 'Bash',
                link: '/windows/commandline/bash/',
              },
              {
                text: 'Dos',
                link: '/windows/commandline/dos/',
              },
              {
                text: 'PowerShell',
                link: '/windows/commandline/powershell/',
              },
            ],
          },
        ],
      },
      {
        text: 'Version',
        items: [
          {
            text: 'Git',
            link: '/version/git/',
          },
        ],
      },
    ],
    sidebar: {
      '/bakend/develop/csharpspec/': getThemeSidebar('CsharpSpec', '介绍'),
      '/bakend/plugins/nlog/': getPluginsSidebar('Nlog', '介绍'),
      '/windows/commandline/powershell/': getPowerShellSidebar('PowerShell', '介绍')
    },
  },

  plugins: [
    //'@vuepress/back-to-top',
    '@vuepress/nprogress',
    '@vuepress/medium-zoom',
    [
      '@vuepress/last-updated',
      {
        transformer: (timestamp, lang) => {
          // 不要忘了安装 moment
          const moment = require('moment')
          moment.locale(lang)
          //YYYY-MM-DD HH:mm
          return moment(timestamp).format('LLLL')
        },
      },
    ],
    'cursor-effects',
    'go-top',
    // [
    //   'vuepress-plugin-helper-live2d',
    //   {
    //     live2d: {
    //       // 是否启用(关闭请设置为false)(default: true)
    //       enable: true,
    //       // 模型名称(default: hibiki)>>>取值请参考：
    //       // https://github.com/JoeyBling/hexo-theme-yilia-plus/wiki/live2d%E6%A8%A1%E5%9E%8B%E5%8C%85%E5%B1%95%E7%A4%BA
    //       model: 'hibiki',
    //       display: {
    //         position: 'right', // 显示位置：left/right(default: 'right')
    //         width: 135, // 模型的长度(default: 135)
    //         height: 300, // 模型的高度(default: 300)
    //         hOffset: 65, //  水平偏移(default: 65)
    //         vOffset: 0, //  垂直偏移(default: 0)
    //       },
    //       mobile: {
    //         show: false, // 是否在移动设备上显示(default: false)
    //       },
    //       react: {
    //         opacity: 0.8, // 模型透明度(default: 0.8)
    //       },
    //     },
    //   },
    // ],
  ],
}

function getThemeSidebar(groupA, introductionA) {
  return [
    {
      title: groupA,
      collapsable: false,
      sidebarDepth: 2,
      children: [['', introductionA], 'int', 'string'],
    },
  ]
}
function getPluginsSidebar(groupA, introductionA) {
  return [
    {
      title: groupA,
      collapsable: false,
      sidebarDepth: 3,
      children: [['', introductionA], 'configuration'],
    },
  ]
}
function getPowerShellSidebar(groupA, introductionA, items) {
  return [
    {
      title: groupA,
      collapsable: false,
      sidebarDepth: 3,
      children: [['', introductionA], 'help-about'],
    },
  ]
}
