# csharpspec

::: tip
语言特性以及函数使用的集锦
:::

## 基本数据类型转换

```csharp
//string类型转成byte[]：

byte[] byteArray = System.Text.Encoding.Default.GetBytes ( str );



//byte[]转成string：

string str = System.Text.Encoding.Default.GetString ( byteArray );



//string类型转成ASCII byte[]：

//（"01" 转成 byte[] = new byte[]{ 0x30,0x31}）

byte[] byteArray = System.Text.Encoding.ASCII.GetBytes ( str );



//ASCIIbyte[]转成string：
（byte[] = new byte[]{ 0x30, 0x31} 转成"01"）

string str = System.Text.Encoding.ASCII.GetString ( byteArray );

//byte[]转16进制格式string：

new byte[]{ 0x30, 0x31}转成"3031":

publicstaticstring ToHexString ( byte[] bytes ) // 0xae00cf => "AE00CF "

{string hexString = string.Empty;

if ( bytes != null )

{

StringBuilder strB = new StringBuilder ();

for ( int i = 0; i < bytes.Length; i++ )

{

strB.Append ( bytes[i].ToString ( "X2" ) );

}

hexString = strB.ToString ();

}return hexString;

}



//16进制格式string 转byte[]：

publicstaticbyte[] GetBytes(string hexString, outint discarded)

{

discarded = 0;

string newString = "";

char c;// remove all none A-F, 0-9, charactersfor (int i=0; i<hexString.Length; i++)

{

  c = hexString[i];if (IsHexDigit(c))

newString += c;

else

discarded++;

}
newString = newString.Substring(0, newString.Length-1);            }

int byteLength = newString.Length / 2;byte[] bytes = newbyte[byteLength];string hex;int j = 0;for (int i=0; i<bytes.Length; i++){

 hex = new String(new Char[] {newString[j], newString[j+1]});

 bytes[i] = HexToByte(hex);                j = j+2;

 }

return bytes;

 }
```

## 反射获取自定义属性

```csharp
void Main()
{
  var item = new Person
  {
    Age = 12
  };
  var tagName = "";

  var m = item.GetType();
  m.Attributes.Dump();
  Attribute.GetCustomAttributes(m).Dump();

  var n = Attribute.GetCustomAttributes(m);

  var apiType = n?.FirstOrDefault(x=>x.GetType()==typeof(ApiListTypeAttribute));
  Console.WriteLine( apiType );

}

// Define other methods, classes and namespaces here
public class ApiListTypeAttribute : Attribute
{
  public String Value { get; set; }

  public ApiListTypeAttribute(string value)
  {
    this.Value = value;
  }
}

[Serializable]
[ApiListType("MyAge")]
public class Person
{
  public int Age { get; set; }
}
```
