# 日期时间格式处理

:::tip
参考链接[cnblog](https://www.cnblogs.com/polk6/p/5465088.html)  
整理一些常见的时间格式处理方法。  
:::

## 英语格式

```csharp
var orderDate = DateTime.Parse(x.OccurTime).ToString("dd-MMM"
, CultureInfo.CreateSpecificCulture("en-US"));
//source : 2020-07-13 星期一  22:32:30,
//result: 13,jul,只显示月份和天数
```

## utc 格式和时区

```csharp
//输出的时间会包括一个`T`标识
var dt = DateTime.Now.Tostring("s");
//标识时区处理，这个需要看下具体的实现
var m = TimeZoneInfo();

```
