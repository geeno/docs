# is 运算符介绍

:::tip
用于类型转换的操作，详细信息  
[is](https://docs.microsoft.com/zh-cn/dotnet/csharp/language-reference/keywords/is)
:::
