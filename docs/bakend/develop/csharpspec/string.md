# String

::: tip
注意：使用 contains 的时候，当匹配字符串为`empty`的时候，也是返回`true`
:::

## Contains(String)

Returns a value indicating whether a specified substring occurs within this string.

```csharp
public bool Contains (string value);
```

`Parameters`

- value

  [String](https://docs.microsoft.com/en-us/dotnet/api/system.string?view=netcore-3.1)

The string to seek.

`Return Value`

- [Boolean](https://docs.microsoft.com/en-us/dotnet/api/system.boolean?view=netcore-3.1)

`true` if the `value` parameter occurs within this string, or if `value` is the empty string (""); otherwise, `false`.

`Exceptions`

[ArgumentNullException](https://docs.microsoft.com/en-us/dotnet/api/system.argumentnullexception?view=netcore-3.1)

`value` is `null`.

## String.IsNullOrWhiteSpace

指示指定的字符串是 `null`、空还是仅由空白字符组成。

C#复制

```csharp
public static bool IsNullOrWhiteSpace (string value);
```

`Parameters`

- value

  [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string?view=netcore-3.1)

要测试的字符串。

`Returns`

- [Boolean](https://docs.microsoft.com/zh-cn/dotnet/api/system.boolean?view=netcore-3.1)

如果 `true` 参数为 `value` 或 `null`，或者如果 [Empty](https://docs.microsoft.com/zh-cn/dotnet/api/system.string.empty?view=netcore-3.1) 仅由空白字符组成，则为 `value`。

`Examples`

下面的示例创建一个字符串数组，然后将数组的每个元素传递到 [IsNullOrWhiteSpace](https://docs.microsoft.com/zh-cn/dotnet/api/system.string.isnullorwhitespace?view=netcore-3.1) 方法。

C#复制运行

```csharp
using System;

public class Example
{
   public static void Main()
   {
      string[] values = { null, String.Empty, "ABCDE",
                          new String(' ', 20), "  \t   ",
                          new String('\u2000', 10) };
      foreach (string value in values)
         Console.WriteLine(String.IsNullOrWhiteSpace(value));
   }
}
// The example displays the following output:
//       True
//       True
//       False
//       True
//       True
//       True
```

## String.IsNullOrEmpty

指示指定的字符串是 `null` 还是空字符串 ("")。

C#复制

```csharp
public static bool IsNullOrEmpty (string value);
```

`Parameters`

- value

  [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string?view=netcore-3.1)

要测试的字符串。

`Returns`

- [Boolean](https://docs.microsoft.com/zh-cn/dotnet/api/system.boolean?view=netcore-3.1)

如果 `true` 参数为 `value` 或空字符串 ("")，则为 `null`；否则为 `false`。

`Examples`

下面的示例检查三个字符串，确定每个字符串是否有值、是否为空字符串或是否 `null`。

C#复制运行

```csharp
string s1 = "abcd";
string s2 = "";
string s3 = null;

Console.WriteLine("String s1 {0}.", Test(s1));
Console.WriteLine("String s2 {0}.", Test(s2));
Console.WriteLine("String s3 {0}.", Test(s3));

String Test(string s)
{
if (String.IsNullOrEmpty(s))
    return "is null or empty";
else
    return String.Format("(\"{0}\") is neither null nor empty", s);
}

// The example displays the following output:
//       String s1 ("abcd") is neither null nor empty.
//       String s2 is null or empty.
//       String s3 is null or empty.
```

`Remarks`

[IsNullOrEmpty](https://docs.microsoft.com/zh-cn/dotnet/api/system.string.isnullorempty?view=netcore-3.1) 是一种简便方法，使您能够同时测试 [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string?view=netcore-3.1) 是否 `null` 或其值是否 [String.Empty](https://docs.microsoft.com/zh-cn/dotnet/api/system.string.empty?view=netcore-3.1)。 它等效于以下代码：

C#复制运行

```csharp
bool TestForNullOrEmpty(string s)
{
    bool result;
    result = s == null || s == string.Empty;
    return result;
}

string s1 = null;
string s2 = "";
Console.WriteLine(TestForNullOrEmpty(s1));
Console.WriteLine(TestForNullOrEmpty(s2));

// The example displays the following output:
//    True
//    True
```

您可以使用 [IsNullOrWhiteSpace](https://docs.microsoft.com/zh-cn/dotnet/api/system.string.isnullorwhitespace?view=netcore-3.1) 方法来测试是否 `null`字符串、是否 [String.Empty](https://docs.microsoft.com/zh-cn/dotnet/api/system.string.empty?view=netcore-3.1)其值，或者它是否仅包含空白字符。

## PadLeft&PadRight

返回一个指定长度的新字符串，其中在当前字符串的开头或者结尾填充空格或指定的 Unicode 字符。

### PadLeft

`OverLoads`

| [PadLeft(Int32, Char)](https://docs.microsoft.com/zh-cn/dotnet/api/system.string.padleft?view=netcore-3.1#System_String_PadLeft_System_Int32_System_Char_) | 返回一个新字符串，该字符串通过在此实例中的字符左侧填充指定的 Unicode 字符来达到指定的总长度，从而使这些字符右对齐。 |
| ---------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------- |
| [PadLeft(Int32)](https://docs.microsoft.com/zh-cn/dotnet/api/system.string.padleft?view=netcore-3.1#System_String_PadLeft_System_Int32_)                   | 返回一个新字符串，该字符串通过在此实例中的字符左侧填充空格来达到指定的总长度，从而实现右对齐。                      |

`Examples`

下面的示例演示 [PadLeft](https://docs.microsoft.com/zh-cn/dotnet/api/system.string.padleft?view=netcore-3.1) 方法。

C#复制运行

```csharp
using System;

class Sample
{
   public static void Main()
   {
   string str = "forty-two";
   char pad = '.';

   Console.WriteLine(str.PadLeft(15, pad));
   Console.WriteLine(str.PadLeft(2, pad));
   }
}
// The example displays the following output:
//       ......forty-two
//       forty-two
```

### PadRight

返回一个指定长度的新字符串，其中在当前字符串的结尾填充空格或指定的 Unicode 字符。

`Overloads`

| [PadRight(Int32, Char)](https://docs.microsoft.com/zh-cn/dotnet/api/system.string.padright?view=netcore-3.1#System_String_PadRight_System_Int32_System_Char_) | 返回一个新字符串，该字符串通过在此字符串中的字符右侧填充指定的 Unicode 字符来达到指定的总长度，从而使这些字符左对齐。 |
| ------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------- |
| [PadRight(Int32)](https://docs.microsoft.com/zh-cn/dotnet/api/system.string.padright?view=netcore-3.1#System_String_PadRight_System_Int32_)                   | 返回一个新字符串，该字符串通过在此字符串中的字符右侧填充空格来达到指定的总长度，从而使这些字符左对齐。                |

`Examples`

下面的示例演示 [PadRight](https://docs.microsoft.com/zh-cn/dotnet/api/system.string.padright?view=netcore-3.1) 方法。

C#复制运行

```csharp
string str = "forty-two";
char pad = '.';

Console.WriteLine(str.PadRight(15, pad));    // Displays "forty-two......".
Console.WriteLine(str.PadRight(2,  pad));    // Displays "forty-two".
```
