# 介绍

::: tip
设计模式整理。
:::

## Factory(工厂模式)

::: tip
主要解决：通过不同的输入参数得到一个共有特性的类，这个类包含了继承关系的子类，  
实现：返回一个父类，从而继续业务逻辑操作。  
`实际例子：比如通过不同的客户ID，定制返回不同的订单模板。`
:::

1.父类

```csharp
public class Calculator
{
    public double NumberA { get; set; }
    public double NumberB { get; set; }
    public virtual double GetResult() => 0;
}
```

2.子类继承

```csharp
public class OperationAdd : Calculator
{
    public override double GetResult()
    {
        return (NumberA + NumberB) * 10;
    }
}
public class OperationSub : Calculator
{
    public override double GetResult()
    {
        return (NumberA - NumberB) * 10;
    }
}
```

3.初始化工厂

```csharp
public class CalculatorFactory
{
    public static Calculator GetCalculator(string operatorChar)
    {
        Calculator result = null;
        switch (operatorChar)
        {
            case "+":
                result = new OperationAdd();
                break;
            case "-":
                result = new OperationSub();
                break;
            default:
                break;
        }
        return result;
    }

}
```

4.调用

```csharp
var op = CalculatorFactory.GetCalculator("+");
op.NumberA = 10;
op.NumberB = 20;
Console.WriteLine(op.GetResult());
```
