# 简单的 Nugget

## CHTCHSConv

:::tip
一个 AspnetCore 下简繁体转换的库。
:::

1.之前 netFrameWork 下使用 `vb` 的两个方法进行转换

```csharp
Strings.StrConv(s_souce, VbStrConv.SimplifiedChinese);//转简体
Strings.StrConv(s_souce, VbStrConv.TraditionalChinese);//转繁体
```

2.`netcore`

```csharp
//nugget安装，然后引用：using Microsoft.International.Converters.TraditionalChineseToSimplifiedConverter;
ChineseConverter.Convert(s_souce, ChineseConversionDirection.TraditionalToSimplified);//转简体
ChineseConverter.Convert(s_souce, ChineseConversionDirection.SimplifiedToTraditional);//转繁体
```

## 判断中文

> [参考](https://www.cnblogs.com/talhon/p/4828793.html)

```csharp
//netcore项目，需要提前安装:System.Text.Encoding.CodePages.dll , CodePage的库，
//并且使用的时候：需要进行注册
//Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

/// <summary>
/// 给定一个字符串，判断其是否只包含有汉字
/// </summary>
/// <param name="testStr"></param>
/// <returns></returns>
public bool IsOnlyContainsChinese(string testStr)
{
char[] words = testStr.ToCharArray();
foreach (char word in words)
{
if ( IsGBCode(word.ToString()) || IsGBKCode(word.ToString()) ) // it is a GB2312 or GBK chinese word
{
continue;
}
else
{
return false;
}
}
return true;
}
/// <summary>
/// 判断一个word是否为GB2312编码的汉字,简体中文
/// </summary>
/// <param name="word"></param>
/// <returns></returns>
private bool IsGBCode(string word)
{
byte[] bytes = Encoding.GetEncoding("GB2312").GetBytes(word);
if (bytes.Length <= 1) // if there is only one byte, it is ASCII code or other code
{
return false;
}
else
{
byte byte1 = bytes[0];
byte byte2 = bytes[1];
if (byte1 >= 176 && byte1 <= 247 && byte2 >= 160 && byte2 <= 254) //判断是否是GB2312
{
return true;
}
else
{
return false;
}
}
}

// <summary>
/// 判断一个word是否为GBK编码的汉字
/// </summary>
/// <param name="word"></param>
/// <returns></returns>
private bool IsGBKCode(string word)
{
byte[] bytes = Encoding.GetEncoding("GBK").GetBytes(word.ToString());
if (bytes.Length <= 1) // if there is only one byte, it is ASCII code
{
return false;
}
else
{
byte byte1 = bytes[0];
byte byte2 = bytes[1];
if ( byte1 >= 129 && byte1 <= 254 && byte2 >= 64 && byte2 <= 254) //判断是否是GBK编码
{
return true;
}
else
{
return false;
}
}
}

/// <summary>
/// 判断一个word是否为Big5编码的汉字
/// </summary>
/// <param name="word"></param>
/// <returns></returns>
private bool IsBig5Code(string word)
{
byte[] bytes = Encoding.GetEncoding("Big5").GetBytes(word.ToString());
if (bytes.Length <= 1) // if there is only one byte, it is ASCII code
{
return false;
}
else
{
byte byte1 = bytes[0];
byte byte2 = bytes[1];
if ( (byte1 >= 129 && byte1 <= 254) && ((byte2 >= 64 && byte2 <= 126) || (byte2 >= 161 && byte2 <= 254)) ) //判断是否是Big5编码
{
return true;
}
else
{
return false;
}
}
}

public void Tets()
{
    unicodeencoding unicodeencoding = new unicodeencoding();
    byte [] unicodebytearray = unicodeencoding.getbytes( inputstring );
    for( int i = 0; i < unicodebytearray.length; i++ )
    {
    i++;
    //如果是中文字符那么高位不为0
    if ( unicodebytearray[i] != 0 )
    {
    }
}

public void Test()
{
    for (int i=0; i<s.length; i++)
{
    Regex rx = new Regex("^[\u4e00-\u9fa5]$");
    if (rx.IsMatch(s[i]))
    // 是
    else
    // 否
    }
   /*
   正解！
    \u4e00-\u9fa5 汉字的范围。
    ^[\u4e00-\u9fa5]$ 汉字的范围的正则
    */
}

public void Test()
{
    /// <summary>
/// 判断句子中是否含有中文(不区分简繁体)
/// </summary>
/// <param >字符串</param>
public bool WordsIScn(string words)
{
string TmmP;

for (int i = 0; i < words.Length; i++)
{
TmmP = words.Substring(i, 1);

byte[] sarr = System.Text.Encoding.GetEncoding("gb2312").GetBytes(TmmP);

if (sarr.Length == 2)
{
return true;
}
}
return false;
}
}


public bool IsChina(string CString)
{
bool BoolValue = false;
for (int i = 0; i < CString.Length; i++)
{
if (Convert.ToInt32(Convert.ToChar(CString.Substring(i, 1))) < Convert.ToInt32(Convert.ToChar(128)))
{
BoolValue = false;
}
else
{
return BoolValue = true;
}
}
return BoolValue;
}

protected bool IsChineseLetter(string input,int index)
 { int code = 0;
 int chfrom = Convert.ToInt32("4e00", 16); //范围（0x4e00～0x9fff）转换成int（chfrom～chend）
 int chend = Convert.ToInt32("9fff", 16);
 if (input != "")
 {
 code = Char.ConvertToUtf32(input, index); //获得字符串input中指定索引index处字符unicode编码

 if (code >= chfrom && code <= chend)
 {
 return true; //当code在中文范围内返回true

 }
 else
 {
 return false ; //当code不在中文范围内返回false
 }
 }
return false;
 }
```
