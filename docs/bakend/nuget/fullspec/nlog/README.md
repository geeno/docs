# Nlog

::: tip
介绍 Nlog 的使用，以下内容，均在：`asp.netcore 2.2下运行测试`。  
[nlog-wiki](https://github.com/NLog/NLog/wiki)  
[github](https://github.com/NLog/NLog)  
[nlogsite](https://nlog-project.org)  
:::

## Runtime

- .NET Framework 3.5, 4, 4.5 - 4.8
- .NET Framework 4 client profile
- Xamarin Android
- Xamarin iOs
- Windows Phone 8
- Silverlight 4 and 5
- Mono 4
- ASP.NET 4 (NLog.Web package)
- ASP.NET Core (NLog.Web.AspNetCore package)
- .NET Core (NLog.Extensions.Logging package)
- .NET Standard 1.x - NLog 4.5
- .NET Standard 2.x - NLog 4.5
- UWP - NLog 4.5

## Asp.netCore 初体验

### 安装 package

`Install-Package NLog.Web.AspNetCore -Version 4.9.2`

### Create a nlog.config file

```xml
<!--配置文件 `nlog.config` 文件的属性为：`copy if newer`-->
<?xml version="1.0" encoding="utf-8" ?>
<nlog
  xmlns="http://www.nlog-project.org/schemas/NLog.xsd"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  autoReload="true"
  internalLogLevel="Info"
  internalLogFile="c:\temp\internal-nlog.txt"
>

  <!-- enable asp.net core layout renderers -->
  <extensions>
    <add assembly="NLog.Web.AspNetCore" />
  </extensions>

  <!-- the targets to write to -->
  <targets>
    <!-- write logs to file  -->
    <target
      xsi:type="File"
      name="allfile"
      fileName="c:\temp\nlog-all-${shortdate}.log"
      layout="${longdate}|${event-properties:item=EventId_Id}|${uppercase:${level}}|${logger}|${message} ${exception:format=tostring}"
    />

    <!-- another file log, only own logs. Uses some ASP.NET core renderers -->
    <target
      xsi:type="File"
      name="ownFile-web"
      fileName="c:\temp\nlog-own-${shortdate}.log"
      layout="${longdate}|${event-properties:item=EventId_Id}|${uppercase:${level}}|${logger}|${message} ${exception:format=tostring}|url: ${aspnet-request-url}|action: ${aspnet-mvc-action}"
    />
  </targets>

  <!-- rules to map from logger name to target -->
  <rules>
    <!--All logs, including from Microsoft-->
    <logger name="*" minlevel="Trace" writeTo="allfile" />

    <!--Skip non-critical Microsoft logs and so log only own logs-->
    <logger
      name="Microsoft.*"
      maxlevel="Info"
      final="true"
    /> <!-- BlackHole without writeTo -->
    <logger name="*" minlevel="Trace" writeTo="ownFile-web" />
  </rules>
</nlog>
```

### Update program.cs

```csharp
using NLog.Web;
using Microsoft.Extensions.Logging;

public static void Main(string[] args)
{
    // NLog: setup the logger first to catch all errors
    var logger = NLog.Web.NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();
    try
    {
        logger.Debug("init main");
        CreateWebHostBuilder(args).Build().Run();
    }
    catch (Exception ex)
    {
        //NLog: catch setup errors
        logger.Error(ex, "Stopped program because of exception");
        throw;
    }
    finally
    {
        // Ensure to flush and stop internal timers/threads before
        //application-exit (Avoid segmentation fault on Linux)
        NLog.LogManager.Shutdown();
    }
}

public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
    WebHost.CreateDefaultBuilder(args)
        .UseStartup<Startup>()
        .ConfigureLogging(logging =>
        {
            logging.ClearProviders();
            logging.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Trace);
        })
        .UseNLog();  // NLog: setup NLog for Dependency injection
```

### Configure appsettings.json

The Logging configuration specified in appsettings.json `overrides` any call to `SetMinimumLevel`. So either remove "Default": or adjust it correctly to your needs.
Remember to also update any environment specific configuration to avoid any surprises.
Ex `appsettings.Development.json`

```json
{
  "Logging": {
    "LogLevel": {
      "Default": "Trace",
      "Microsoft": "Information"
    }
  }
}
```

### Write logs

通过 `Ilogger` 实现注入，使用。

```csharp
using Microsoft.Extensions.Logging;

public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;

    public HomeController(ILogger<HomeController> logger)
    {
        _logger = logger;
    }

    public IActionResult Index()
    {
        _logger.LogInformation("Index page says hello");
        return View();
    }
```
