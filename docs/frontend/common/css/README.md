# Css

## CSS 样式优先级权重

总的计算值大的会优先执行对应的样式设置效果。

![An image](../../../.vuepress/public/img/css01.png)

## 浮动

1.任何标签，添加`style="float:xx"`属性之后，都会变成`块级`标签.

2.清除浮动

- 添加新的标签并且加上样式`clear:both`

- 父级标签添加`overflow:hidden`(首选)

```txt
实现清除,超出部分隐藏，所以就不会影响下边的标签了
```

- 使用伪元素

```css
父级元素:after:{
content："";
display:block;
clear:both;

```

## 基本布局

- 左中右

```html

```

## z-index

> z-index 属性，只适用于 position:absolute 的元素，值越大的在上一层
