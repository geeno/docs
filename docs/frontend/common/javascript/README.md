# javascript

::: tip
整理一下易错的知识点。
:::

## 遍历

- **for...in...**

```js
//迭代对象（集合，数组）等。

/*
for (const key in gp) {
          if (gp.hasOwnProperty(key)) {
            const element = gp[key];
            console.log(element);
          }
        }
*/
var aryObj = []
aryObj.push({ name: 'jack' })
aryObj.push({ name: 'jack2' })
//这种数组对象的话就采用for...in...进行遍历就行了.

var obj = {}
obj.aryObj = []
obj.aryObj.push({ name: 'jack' })

//这种数组对象的话就采用for...in...进行遍历就行了.
```

- **for...**

```js
//迭代遍历数组元素
for (let index = 0; index < array.length; index++) {
  const element = array[index]
}
```

## Form-Serialize

**注意事项**：
> 在javascript中，可以利用表单字段的type属性，连同name和value属性一起实现对表单的序列化，序列化后将把这些数据发送给服务器。
下面是将那些字段需要进行序列化的；

```txt
1. 对表单字段的名称和值进行URL编码，使用&分割；
2. 不发送禁用的表单字段；
3. 只发送勾选的单选框和复选框按钮数据；
4. 不发送type为reset和button的按钮
5. 多选选择框中的每个选中的值单独一个条目；
6. Select元素的值，就是选中option元素的value的值，如果option没有属性value，则是选中的文本值；
```

```js
//使用 
var m = $('frm').serializeArray();
  var m = $('frm').serialize();
  /**
   * 将Form的数据转化成Javascript的Json对象
   */
  $.fn.serializeObject = function(){
      var o = {};
      var a = this.serializeArray();
      $.each(a, function() {
          if (o[this.name] !== undefined) {
              if (!o[this.name].push) {
                  o[this.name] = [o[this.name]];
              }
              o[this.name].push(this.value || '');
          } else {
              o[this.name] = this.value || '';
          }
      });
      return o;
  }
      
   
  //方法3：serializeObject
  var obj = $('form').serializeObject();
```


