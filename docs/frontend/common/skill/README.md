# 整理开发技巧

## 网站音频播放

- 直接播放音频文件

  > [howler](https://github.com/goldfire/howler.js)

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
    <script src="./assets/lib/howler.min.js"></script>
  </head>
  <body>
    <button id="test">test</button>
    <script>
      document.getElementById('test').onclick = function () {
        var sound = new Howl({
          src: ['/assets/media/1.wav'],
          autoplay: true,
          onend: function () {
            console.log('Finished!')
          },
        })
      }
    </script>
  </body>
</html>
```

- 调用浏览器 API 实时播放

> [参考链接](https://developer.mozilla.org/zh-CN/docs/Web/API/SpeechSynthesis)

```js
//这是实验性的功能，2020-09-12 星期六  10:53:01
//还在部分浏览器进行试验中。
var synth = window.speechSynthesis
var utterThis = new SpeechSynthesisUtterance(123)
synth.speak(utterThis)
```

## script 标签实现插件引入

```html
<head>
  <script
    src="/script/picSwitch.js"
    id="picSwitch"
    obj="#pic"
    option='{"speed":"400","derection":"left"}'
  ></script>
</head>
<body>
  <div id="pic">
    //这里是具体结构
  </div>
</body>
```

```js
$.fn.picSwitch = function(){
            //这里是图片切换的代码

};

//写好插件后就直接调用
  var script = $('#picSwitch')，//标签上的id
    selector = script.attr('selector'),
    option = JSON.parse(script.attr('option'));//标签上的是字符串需要转为json对象
    $(selector).picSwitch(option);
```

## 正则表达式生成

> 使用`FluentAPI`的方式，生成正则表达式  
> [参考](https://github.com/francisrstokes/super-expressive)
