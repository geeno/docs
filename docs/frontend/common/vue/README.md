# Vue

::: tip
整理 vue 使用的相关知识点。
:::

## vue 组件使用

- 所有的组件都必须写在 app 标签内

## vue 结合 jquery-valiate 进行验证

```js
$(function () {
  var id = 0
  var app = new Vue({
    el: '#detailForm',
    data: {
      radSpec: '1',
      chkResult: [],
    },
    methods: {
      addChkEntity: function () {
        var validator = $('#frmSockinCheckOrder').validate()
        var validRlt = validator.form()
        if (validRlt) {
          var obj = {}
          obj.ID = ++id
          obj.BoxNbr = 'test00' + id
          this.chkResult.push(obj)
        }
      },
    },
  })
})
```
