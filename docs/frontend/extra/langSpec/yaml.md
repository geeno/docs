# yaml 语法介绍

::: tip
使用 yaml 语法简化一些 js 对象操作
[offical site](https://yaml.org/spec/1.2/spec.html)  
[online demo](http://nodeca.github.io/js-yaml/)  
[阮一峰博客-yaml](http://www.ruanyifeng.com/blog/2016/07/yaml.html)
:::
