# 介绍

::: tip
一些常用的第三方 lib 的使用方法整理。  
简单的使用的插件的话，不再单独一个文件去说明，直接整合到`Readme`里。
:::

## underscore

> [official site](https://underscorejs.org/)

:::warning
`作为学习JS的一个框架`。
此库不依赖其他任何 js 库，却支持了很多实用的函数，所以在处理浏览器兼容方法的时候，
这个绝对是首选项，或者都直接引用这个库就行(不考虑兼容)。
:::

:::tip
Underscore.js is a utility-belt library for JavaScript that provides support for the usual functional suspects (each, map, reduce, filter...) without extending any core JavaScript objects.

For Docs, License, Tests, and pre-packed downloads, see: `https://underscorejs.org`

For support and questions, please use the gitter channel or stackoverflow

Underscore is an open-sourced component of DocumentCloud: `https://github.com/documentcloud`

Many thanks to our contributors: `https://github.com/jashkenas/underscore/contributors`

This project adheres to a code of conduct. By participating, you are expected to uphold this code.
:::

- _\_.reduce`实现汇总计算,主要针对`对象_

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
    <script src="../underscore-min.js"></script>
  </head>
  <body>
    <script>
      var ary = []
      ary.push({
        name: 'jack',
        age: 12,
      })
      ary.push({
        name: 'jack',
        age: 22,
      })
      var m = _.reduce(
        ary,
        function (memo, next) {
          return memo + next.age
        },
        //memo 代表着第一个计算值，这是必须的
        0,
      )
      console.log(m)
    </script>
  </body>
</html>
```

- **\_.groupBy(list, iteratee, [context])**

> 对数据进行分组操作，主要注意多个字段进行分组的话，采用`字符串拼接方式`进行即可。
> 关联参考[`_.pluck`(返回集合指定映射某个字段信息),`_.size`(返回 list 的数量)]  
> Splits a collection into sets, grouped by the result of running each value through `iteratee`.  
> If `iteratee is` a string instead of a function, groups by the property named by `iteratee` on each of the values.

```js
var list = [
  {
    user_id: 301,
    alert_id: 199,
    deal_id: 32243,
  },
  {
    user_id: 301,
    alert_id: 200,
    deal_id: 32243,
  },
  {
    user_id: 301,
    alert_id: 200,
    deal_id: 107293,
  },
  {
    user_id: 301,
    alert_id: 200,
    deal_id: 277470,
  },
]

//根据指定单个属性进行分组
var tmp2 = _.groupBy(list, 'alert_id')

//指定多个字段分组
var groups = _.groupBy(list, function (value) {
  return value.user_id + '#' + value.alert_id
})
console.log(groups)
//解析分组结果
var data = _.map(groups, function (group) {
  return {
    user_id: group[0].user_id,
    alert_id: group[0].alert_id,
    deals: _.pluck(group, 'deal_id'),
  }
})
console.log(data)
```

## bootBox

> [official site](http://bootboxjs.com/)

`弹出层展示`

```js
var dialog = bootbox.dialog({
  message:
    '<div class="text-center"><i class="fa fa-spin fa-spinner"></i> Loading...</div>',
  closeButton: false,
})

//hide
Dialog.modal('hide')
//show
Dialog.modal('show')
```

## bootstrapDatePicker

> [official site](https://uxsolutions.github.io/bootstrap-datepicker/?markup=input&format=&weekStart=&startDate=&endDate=&startView=0&minViewMode=0&maxViewMode=4&todayBtn=false&clearBtn=false&language=en&orientation=auto&multidate=&multidateSeparator=&keyboardNavigation=on&forceParse=on#sandbox)

`初始化`

```html
<div class="form-group">
  <label class="control-label">@PageLocale.CreateDate</label>
  <div
    class="input-group  date-picker input-daterange"
    data-date-format="yyyy-mm-dd"
  >
    <input type="text" id="_occurTimeStart" name="_occurTimeStart"
    value="@DateTime.Now.AddDays(-6).ToString("yyyy-MM-dd")"
    class="form-control" style="font-size:10px;">
    <span class="input-group-addon">
      to
    </span>
    <input type="text" class="form-control" style="font-size:10px;"
    id="_occurTimeEnd" name="_occurTimeEnd"
    value="@DateTime.Now.ToString("yyyy-MM-dd")">
  </div>
</div>
```

```js
function initDatePicker(alignment) {
  $('.date-picker').datepicker({
    orientation: alignment || 'left',
    language: 'zh-CN',
    autoclose: true,
    todayHighlight: true,
  })
}
```
