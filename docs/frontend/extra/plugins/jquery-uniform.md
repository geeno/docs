# Jquery-uniform

:::tip
一款统一表单的呈现方式的插件。
:::

## uniform.update(xx)

实现勾选框的状态更新，如果是动态勾选的话，必须要使用这个方法  
进行更新控件显示状态。

::: warning
基本语法：  
`$.uniform.update([elem/selector string]);`

If you need to change values on the form dynamically you must tell Uniform to update that element’s style. Fortunately, it’s very simple. Just call this function, and Uniform will do the rest.
`$.uniform.update("#myUpdatedCheckbox");`

If you don't mind updating all Uniformed elements or just don’t specifically know which element to update, you can just leave out the parameter (see below) and Uniform will update all Uniformed elements on the page:  
`$.uniform.update();`
:::
