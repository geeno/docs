# Jquery-Validation

## 1. online doc

> 在线 Git:
> [onlineGit](https://github.com/jquery-validation/jquery-validation)

## 2. 依赖

> 依赖：
> Required
> jQuery, tested with 1.7.2, 1.8.3, 1.9.1, 1.11.1, 3.1.1
> 必须在 form 里使用 submit 按钮，并且属性必须有 Name

## 3. 配置

### 3.1. 错误信息操作

```js
/*
All validation rules included here provide a default error message
which you can use for prototyping, because it is used
when no specific message is provided.
*/
//错误信息显示：优先级-最优到最低：
//The priorities are as follows:
//1. A custom message (passed by plugin options)
    messages:{
      name:"Name不能为空",
      email:{
          required:"E-mail不能为空",
          email:"E-mail地址不正确"
      }
  }
//2. the element's title, --可以使用titile进行默认错误信息设置
let title='<a title="message"></a>';
//3. the default message.
//系统初始化的消息

//配置的错误信息-样式
$("#commentForm").validate({errorClass:"errorPrompt"});
//通过errorClass属性实现错误信息样式显示调整位置上的调整：参考errorPlacement
```

### 3.2. 支持 data 属性配置显示

```html
<!--
指定校验规则对应的错误信息：
When using data attributes, you can set a generic message for all rules,
or specific messages per rule
-->
<!-- 指定校验规则以及错误信息提示 -->
<input
  data-rule-minlength="2"
  data-rule-maxlength="4"
  data-msg-minlength="At least two chars"
  data-msg-maxlength="At most fours chars"
/>
```

### 3.3. 窗体提交处理

```js
/*
窗体提交---提交之后进行处理ajax等信息：submitHandler
Form submit
By default, the form submission is prevented when the form is invalid,
and submitted as normal when it is valid. You can also handle the submission manually
*/
//验证通过之后，才会执行submitHandler的function里的操作
$('#form').validate({
  submitHandler: function (form) {
    //todo valid operation
    $(form).ajaxSubmit()
  },
})

//第二种方式
var validator = $('#frmSockinCheckOrder').validate()
var validRlt = validator.form()
if (validRlt) {
  var obj = {}
  obj.ID = ++id
  obj.BoxNbr = 'test00' + id
  this.chkResult.push(obj)
}
```

### 3.4. 自定义验证属性

```js
//添加的方法都支持：data属性操作
$.validator.addMethod(
  'leQty',
  function (value, element, param) {
    console.dir(value) //当前输入值
    console.log(element) //当前验证的元素-DOM对象
    console.dir(param) //Parma通过调用传入的参数信息,可以是数组或者单个数值
    return this.optional(element) || Number(value) <= Number(param)
  },
  $.validator.format('输入数据<=可用数量{0}'),
) //{0}就代表第一个参数

//输入小数或者整数
$.validator.addMethod(
  'decimalNumber',
  function (value, element, params) {
    return this.optional(element) || /(^\d+[.]?\d+$)|(^\d+$)/.test(value)
  },
  'greater than or equal 0.',
)
```

### 3.5. 重置窗体状态

```js
var validator = $('#myform').validate()
validator.resetForm()
```

### 3.6. 内置的校验规则

- [`required` – Makes the element required.](https://jqueryvalidation.org/required-method)

- [`remote` – Requests a resource to check the element for validity.](https://jqueryvalidation.org/remote-method)

- [`minlength` – Makes the element require a given minimum length.](https://jqueryvalidation.org/minlength-method)

- [`maxlength` – Makes the element require a given maximum length.](https://jqueryvalidation.org/maxlength-method)

- [`rangelength` – Makes the element require a given value range.](https://jqueryvalidation.org/rangelength-method)

- [`min` – Makes the element require a given minimum.](https://jqueryvalidation.org/min-method)

- [`max` – Makes the element require a given maximum.](https://jqueryvalidation.org/max-method)

- [`range` – Makes the element require a given value range.](https://jqueryvalidation.org/range-method)

- [`step` – Makes the element require a given step.](https://jqueryvalidation.org/step-method)

- [`email` – Makes the element require a valid email](https://jqueryvalidation.org/email-method)

- [`url` – Makes the element require a valid url](https://jqueryvalidation.org/url-method)

- [`date` – Makes the element require a date.](https://jqueryvalidation.org/date-method)

- [`dateISO` – Makes the element require an ISO date.](https://jqueryvalidation.org/dateISO-method)

- [`number` – Makes the element require a decimal number.](https://jqueryvalidation.org/number-method)

- [`digits` – Makes the element require digits only.](https://jqueryvalidation.org/digits-method)

  只能是 0~的正整数

- [`equalTo` – Requires the element to be the same as another one](https://jqueryvalidation.org/equalTo-method)

需要更多的规则，参看 addtional.js 文件，或者自定义规则

## 最终示例

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Document</title>
    <script src="./jquery.min.js"></script>
    <script src="./jquery-validation-1.13/js/jquery.validate.min.js"></script>
    <style>
      .errorPrompt {
        color: red;
      }
    </style>
  </head>

  <body>
    <form id="commentForm" method="get" action="">
      <fieldset>
        <legend>
          Please provide your name, email address (won't be published) and a
          comment
        </legend>
        <p>
          <label for="cName">Name (required, at least 2 characters)</label>
          <input
            id="cName"
            name="name"
            data-rule-maxlength="4"
            data-msg-maxlength="最大4"
            data-msg-minlength="最小2"
            data-rule-minlength="2"
            type="text"
            required
          />
        </p>
        <p>
          <input class="submit" type="submit" value="Submit" />
        </p>
      </fieldset>
    </form>

    <script>
      $(function () {
        $('#commentForm').validate({
          errorClass: 'errorPrompt',
          submitHandler: function (form) {
            // do other things for a valid form
            form.submit()
            //这里执行业务逻辑处理
          },
          debug: true,
          ignore: '.ignore', //绑定忽略验证项
        })
      })
      /*
    $(".selector").validate({
        rules:{
            name:"required",
            email:{
                required:true,
                email:true
            }
        },
        messages:{
            name:"Name不能为空",
            email:{
                required:"E-mail不能为空",
                email:"E-mail地址不正确"
            }
        }
    })
    */
    </script>
  </body>
</html>
```
