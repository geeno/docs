# Jquery

:::tip
jquery 的知识整理。  
[Jquery](https://api.jquery.com/)
:::

## .serialize()

参考链接:[serialize](https://api.jquery.com/serialize/)  
:::warning
`勾选框需要选中的时候，才会获取值进行传递：`  
For a form element's value to be included in the serialized string,  
the element must have a name attribute. Values from checkboxes and radio buttons  
(inputs of type "radio" or "checkbox") are included only if they are checked.  
Data from file select elements is not serialized.
:::

## .val()

> 1. when called on an empty collection, it returns `undefined`.
>
>    当没有匹配到对应元素的时候比如:`$('#kkk').val()`
>
> 2. When the first element in the collection is a `select-multiple` (i.e., a `select` element with the `multiple` attribute set), `.val()` returns an array containing the value of each selected option. **As of jQuery 3.0**, if no options are selected, it returns an empty array; **prior to jQuery 3.0**, it returns `null`.
>
>    当前使用的是`jq1.12.x`所以，如果是下拉框选择的数据的话，无数据--option的直接返回：`null`