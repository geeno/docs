# welcome

## image

![An image](../.vuepress/public/img/download.png)

## message

::: danger
hehehe
:::

::: tip
hehehe
:::

::: warning
gege
:::

## emoji

[emoji](https://github.com/markdown-it/markdown-it-emoji/blob/master/lib/data/full.json)

:+1::+1::+1::+1::+1:

## code highlighting

```js
function hello() {
  console.log('hello darling')
}
function hello() {
  console.log('hello darling')
}
function hello() {
  console.log('hello darling')
}
function hello() {
  console.log('hello darling')
}
function hello() {
  console.log('hello darling')
}
```
