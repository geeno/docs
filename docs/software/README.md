# 介绍

::: tip
整理一些开发中使用到的软件的一些注意事项，以及使用方法。
:::

## 音乐播放器

-[listen](https://listen1.github.io/listen1/)

一款全面整合了多个平台音乐的软件。

## BeCompare

`软件版本`: `4.2.10`
[官网](http://www.scootersoftware.com/)

> 6TTCoWi2N0Pv+o2HGfqUpZfuaMhtf2zX0u1OuNeqTYkKKWh-CKwBWkPUG
> 3+CiAQ2q4MNPbf0t8+gmPdo+Vyw64aU-zuQQt9d7Q6EcJ+T42by0E+kxf
> +q3QLs40H+RD3h5OLjFGpxClodRnTCNoAM39xsWm2aHZI0Z9KdXzLo1fo
> 1OdNlaptoK17SsxNK-7JUtTztLwBM8BUwWA24ghoeLhFq39FMP+pcdU7R
> ttFJoosVk3d-DRrDH0EARo6GXWEeeUgnyjdWKv5ElwrHWw2HMpfFq9VRf
> MqcJV00ePAUB4MT3zPE43Tu0BDt-WXYSpUqf7AjaWnBC7MpNfdkS0mWXk
