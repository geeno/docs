# MarkDown 语法

## 任务列表

```html
- [ ] 代表一个复选框(未选中的状态) , - [x] 代表一个复选框（已经选中)
```

## Horizontal Rules

Entering \*\*\* or --- on a blank line and pressing return will draw a horizontal line.

## Links

- outer link
  `[xxx](http://www.baidu.com)`

- internal link
  `[yyy](#Links)`

## Image

```html
![xx](xx.jpg)
```
