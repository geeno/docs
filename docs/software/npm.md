# NPM使用

## 配置淘宝镜像

```txt
--永久设置淘宝镜像
$ npm config set registry https://registry.npm.taobao.org

--查看配置结果
$ npm config get registry

--恢复初始镜像
 npm config set registry https://registry.npmjs.org
```

## Node环境变量配置

```txt
--使用环境变量就是为了解决部分工具只认识环境变量名的情况
1. 新建环境变量：
名字：NODE_PATH
路径：Nodejs的安装目录

2. 在环境变量【path】里新建节点：%NODE_PATH%

3. 重新进入控制台或者其他命令行工具就行
```