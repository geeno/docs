# visual studio

## RazorTagHelper

> 更新 VS 2019 16.8.0 之后运行 aspnetcore2.2web 项目会出现异常提示信息

```text
After updating VS 2019 16.8.0 I'm facing this error on compilation on any project:
Web C# .net core 2.2
`Erro MSB4018 Falha inesperada da tarefa "RazorTagHelper"`.
System.InvalidOperationException: DOTNET_HOST_PATH is not set
em Microsoft.AspNetCore.Razor.Tasks.DotNetToolTask.get_DotNetPath()
em Microsoft.AspNetCore.Razor.Tasks.DotNetToolTask.GenerateFullPathToTool()
em Microsoft.Build.Utilities.ToolTask.ComputePathToTool()
em Microsoft.Build.Utilities.ToolTask.Execute()
em Microsoft.AspNetCore.Razor.Tasks.DotNetToolTask.Execute()
em Microsoft.Build.BackEnd.TaskExecutionHost.Microsoft.Build.BackEnd.ITaskExecutionHost.Execute()
em Microsoft.Build.BackEnd.TaskBuilder.d\_\_26.MoveNext() QRPlus C:\Program Files\dotnet\sdk\NuGetFallbackFolder\microsoft.aspnetcore.razor.design\2.2.0\build\netstandard2.0\Microsoft.AspNetCore.Razor.Design.CodeGeneration.targets 79
```

`解决方案`

> 增加引用包就可以正常运行了

```text
Adding this line in project file works or install razor package:
<PackageReference Include="Microsoft.NET.Sdk.Razor" Version="2.2.0"></PackageReference>
```
