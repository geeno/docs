# git

## git 快速使用

> [从入门到熟练使用](https://www.jianshu.com/p/34cfe097e06a)  
> [教你系统学习 Git](https://www.jianshu.com/p/fe038a97bb3c)

## 后悔药

- `git amend`覆盖提交信息

重新提交：有时候我们提交完了才发现漏掉了几个文件没有添加，或者提交信息写错了。  
此时，可以运行带有 --amend 选项的提交命令尝试重新提交：\$ git commit --amend 这个命令将暂存区中的文件提交，
如果自从上次提交以来还未做任何修改，则快照保持不变，你修改的只有提交信息。  
例如你提交后发现忘记了暂存某些需要的修改，可以像下面这样操作：

```shell
$ git commit -m 'initial commit'
$ git add forgotten_file
$ git commit --amend

# 最终只会有一个提交，第二次提交将代替第一次提交的结果
```

## .ignore 文件配置

1.匹配语法

::: tip
• 所有空行或者以 # 开头的行都会被 Git 忽略。  
• 可以使用标准的 glob 模式匹配，它会递归地应用在整个工作区中。  
• 匹配模式可以以（/）开头防止递归。  
• 匹配模式可以以（/）结尾指定目录。  
• 要忽略指定模式以外的文件或目录，可以在模式前加上叹号（!）取反。  
:::

2.匹配文件示例

```shell
# 忽略所有的 .a 文件
*.a
# 但跟踪所有的 lib.a，即便你在前面忽略了 .a 文件
!lib.a
# 只忽略当前目录下的 TODO 文件，而不忽略 subdir/TODO
/TODO
# 忽略任何目录下名为 build 的文件夹
build/
# 忽略 doc/notes.txt，但不忽略 doc/server/arch.txt
doc/*.txt
# 忽略 doc/ 目录及其所有子目录下的 .pdf 文件
doc/**/*.pdf
```

## 查看某行代码的更改记录

`git log`

```shell
# git log -L 1,2:xx.txt
$git log -L beginLine,endLine:filename
```

## 同步更新 fork 的代码

> [参考链接](https://blog.csdn.net/qq1332479771/article/details/56087333)
