# 注册表介绍

:::tip
整理一些注册表操作的常用知识。

`HKEY_CLASSES_ROOT\*`：系统所有文件，右键系统任一文件都会添加右键菜单  
`HKEY_CLASSES_ROOT\AllFilesystemObjects`：系统所有文件和文件夹，右键任一文件或者文件夹都会添加右键菜单  
`HKEY_CLASSES_ROOT\Folder`：系统所有文件夹，右键系统任一文件夹都会添加右键菜单  
`HKEY_CLASSES_ROOT\Directory`：系统所有目录，右键系统任一文件夹都会添加右键菜单  
`HKEY_CLASSES_ROOT\Directory\Background`：系统文件夹空白处右键，在文件夹内空白处右键都会添加右键菜单  
:::

## 实现把程序添加到右键菜单操作

只需要把应用程序路径填入替换就行了。

```shell
Windows Registry Editor Version 5.00

[HKEY_CLASSES_ROOT\*\shell\VSCode]
@="Open with Code"
"Icon"="C:\\Microsoft VS Code\\Code.exe"

[HKEY_CLASSES_ROOT\*\shell\VSCode\command]
@="\"C:\\Microsoft VS Code\\Code.exe\" \"%1\""

Windows Registry Editor Version 5.00

[HKEY_CLASSES_ROOT\Directory\shell\VSCode]
@="Open with Code"
"Icon"="C:\\Microsoft VS Code\\Code.exe"

[HKEY_CLASSES_ROOT\Directory\shell\VSCode\command]
@="\"C:\\Microsoft VS Code\\Code.exe\" \"%V\""

Windows Registry Editor Version 5.00

[HKEY_CLASSES_ROOT\Directory\Background\shell\VSCode]
@="Open with Code"
"Icon"="C:\\Microsoft VS Code\\Code.exe"

[HKEY_CLASSES_ROOT\Directory\Background\shell\VSCode\command]
@="\"C:\\Microsoft VS Code\\Code.exe\" \"%V\""
```
