# 系统盘空间释放

## 调整.nuget 的缓存位置

> [参考如何移动 nuget 缓存文件夹](https://blog.csdn.net/lindexi_gd/article/details/79399744)

这个`.nuget`文件夹的缓存路径`C:\Users\GeenoLi\.nuget\packages`

- 直接更改配置文件信息

打开 `%AppData%\NuGet\NuGet.Config` ，在这个文件夹添加下面代码  
对于我的电脑而言目录为`C:\Users\GeenoLi\AppData\Roaming\NuGet`

```xml
<?xml version="1.0" encoding="utf-8" ?>
<configuration>
  <packageSources>
    <add
      key="nuget.org"
      value="https://api.nuget.org/v3/index.json"
      protocolVersion="3"
    />
    <add key="ZebraCore" value="E:\WorkFiles\Zebra\Git_Projects\WO\LIB" />
    <add
      key="Microsoft Visual Studio Offline Packages"
      value="C:\Program Files (x86)\Microsoft SDKs\NuGetPackages\"
    />
  </packageSources>
  <!--这里添加这个节点就行-->
  <config>
    <add key="globalPackagesFolder" value="E:\DevFilesAndStudy\packages" />
  </config>
   <!--这里添加这个节点就行-->
</configuration>
```
