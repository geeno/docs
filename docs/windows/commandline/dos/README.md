# dos

## Network

> 1. `nslookup xx域名`
>
>    查看对应域名的**IP**地址信息和 DNS 解析信息
>
> 2. `ipconfig`
>
>    查看网络配置
>
>    `/all`: 查看所有 ip 地址信息包括，dns 等
>
>    `/release`: 释放获取到的 DHCP 信息
>
>    `/renew`: 重新获取 DHCP 信息
>
>    [^renew]:有 IP 的情况下，执行续约操作, 无 IP 的情况下重新获取
>
>    `displaydns`: 显示本地已经解析过的 dns 记录

## File

> 1. `mkdir xxname`
>
>    创建文件夹
>
> 2. `dir`
>
>    列出当前路径下的所有**文件夹和文件**信息
>
> 3. `ls`
>
>    列出当前路径下的**文件**信息
>
>    `-ah` ：显示所有**隐藏**的文件

## UserManage

> 1. `net user xxname /add`
>
>    添加一个普通用户
>
>    `net user xxname xxpwd` : 创建用户并设置密码
>
>    其他使用：net user /?
>
> 2. `whoami /user`
>
>    查看当前用户名称和 SID
>
>    [^sid]:Security Identifiers，SID,安全标识符，安全标识符（Security Identifiers，SID），是标识用户、组和计算机帐户的唯一的号码。
>
> 3. `net localgroup`
>
>    查看本地用户组
>
> 4. `net share`
>
>    查看当前共享的文件标识符
>
>    => 系统会默认启动-共享**C\\\\\\\\\\$,D\$**这样子的话，会有些黑客会通过这样的共享方式
>
>    进行访问磁盘的操作不安全，所以需要实现：禁用这样子的默认共享
>
> 打开注册表。在 HKEY_LOCAL_MACHINE \SYSTEM \CurrentControlSet \Services \lanmanserver \parameters 下建立一个 DWORD 值，
>
> > 将**AutoShareServer**和**AutoShareWks**数值配置为 0 即可!

## Shutdown-系统定时关机命令

> 1. 在指定时间执行关机操作
>
>    **at 00:00 shutdown -s** (00:00 就是对应的时间的 24h 制)。
>
> 2. 取消关机命令
>
>    `shutdown -a` 。
>
> 3. 在多少秒之后执行关机
>
>    `shutdown -s -t 60` =>在 60s 之后执行关机操作。
>
> 4. win7~win10-已经弃用 at,所以得使用 schtasks-在服务里需要启动这个 task 的服务
>
>    在控制台里执行这个目前需要**关掉提示的弹出框才会执行**：
>
>    开启：`schtasks /create /tn “关机” /tr “shutdown /s” /sc once /st 22:40`，
>
>    删除：`schtasks /delete /tn “关机”`。

## 查看端口占用

1.输入命令：`netstat -ano`，列出所有端口的情况。在列表中我们观察被占用的端口，比如是 49157，首先找到它.  
2.查看被占用端口对应的 PID，输入命令：`netstat -aon|findstr "49157"`，回车，记下最后一位数字，即 PID,这里是 2720。  
3.继续输入`tasklist|findstr "2720"`，回车，查看是哪个`进程或者程序`占用了 2720 端口，结果是：svchost.exe

## 添加右键菜单处理

:::warning
这里的`单斜杠`和`双斜杠`一定要和下面的信息匹配，特别注意下，不然配置不会生效。
:::

```shell
Windows Registry Editor Version 5.00

[HKEY_CLASSES_ROOT\*\shell\VSCode]
@="Open with Code"
"Icon"="C:\\Users\\LiTao\\AppData\\Local\\Programs\\Microsoft VS Code\\Code.exe"

[HKEY_CLASSES_ROOT\*\shell\VSCode\command]
@="\"C:\\Users\\LiTao\\AppData\\Local\\Programs\\Microsoft VS Code\\Code.exe\" \"%1\""

Windows Registry Editor Version 5.00

[HKEY_CLASSES_ROOT\Directory\shell\VSCode]
@="Open with Code"
"Icon"="C:\\Users\\LiTao\\AppData\\Local\\Programs\\Microsoft VS Code\\Code.exe"

[HKEY_CLASSES_ROOT\Directory\shell\VSCode\command]
@="\"C:\\Users\\LiTao\\AppData\\Local\\Programs\\Microsoft VS Code\\Code.exe\" \"%V\""

Windows Registry Editor Version 5.00

[HKEY_CLASSES_ROOT\Directory\Background\shell\VSCode]
@="Open with Code"
"Icon"="C:\\Users\\LiTao\\AppData\\Local\\Programs\\Microsoft VS Code\\Code.exe"

[HKEY_CLASSES_ROOT\Directory\Background\shell\VSCode\command]
@="\"C:\\Users\\LiTao\\AppData\\Local\\Programs\\Microsoft VS Code\\Code.exe\" \"%V\""
```
