# About 文档索引

:::tip
使用：`help aboutxxx`就行了.
:::

| #TYPE HelpInfoShort                    |          |                                                                                              |
| -------------------------------------- | -------- | -------------------------------------------------------------------------------------------- |
| Name                                   | Category | Synopsis                                                                                     |
| about_ActivityCommonParameters         | HelpFile | Describes the parameters that Windows PowerShell                                             |
| about_Aliases                          | HelpFile | Describes how to use alternate names for cmdlets and commands in Windows                     |
| about_Arithmetic_Operators             | HelpFile | Describes the operators that perform arithmetic in Windows PowerShell.                       |
| about_Arrays                           | HelpFile | Describes arrays, which are data structures designed to store                                |
| about_Assignment_Operators             | HelpFile | Describes how to use operators to assign values to variables.                                |
| about_Automatic_Variables              | HelpFile | Describes variables that store state information for Windows PowerShell.                     |
| about_Break                            | HelpFile | Describes a statement you can use to immediately exit Foreach, For, While,                   |
| about_Checkpoint-Workflow              | HelpFile | Describes the Checkpoint-Workflow activity, which                                            |
| about_CimSession                       | HelpFile | Describes a CimSession object and the difference between CIM sessions and                    |
| about_Classes                          | HelpFile | Describes how you can use classes to develop in Windows PowerShell                           |
| about_Command_Precedence               | HelpFile | Describes how Windows PowerShell determines which command to run.                            |
| about_Command_Syntax                   | HelpFile | Describes the syntax diagrams that are used in Windows PowerShell.                           |
| about_Comment_Based_Help               | HelpFile | Describes how to write comment-based help topics for functions and scripts.                  |
| about_CommonParameters                 | HelpFile | Describes the parameters that can be used with any cmdlet.                                   |
| about_Comparison_Operators             | HelpFile | Describes the operators that compare values in Windows PowerShell.                           |
| about_Continue                         | HelpFile | Describes how the Continue statement immediately returns the program flow                    |
| about_Core_Commands                    | HelpFile | Lists the cmdlets that are designed for use with Windows PowerShell                          |
| about_Data_Sections                    | HelpFile | Explains Data sections, which isolate text strings and other read-only                       |
| about_Debuggers                        | HelpFile | Describes the Windows PowerShell debugger.                                                   |
| about_DesiredStateConfiguration        | HelpFile | Provides a brief introduction to the Windows                                                 |
| about_Do                               | HelpFile | Runs a statement list one or more times, subject to a While or Until                         |
| about_Environment_Variables            | HelpFile | Describes how to access Windows environment variables in Windows                             |
| about_Escape_Characters                | HelpFile | Introduces the escape character in Windows PowerShell and explains                           |
| about_Eventlogs                        | HelpFile | Windows PowerShell creates a Windows event log that is                                       |
| about_Execution_Policies               | HelpFile | Describes the Windows PowerShell execution policies and explains                             |
| about_For                              | HelpFile | Describes a language command you can use to run statements based on a                        |
| about_ForEach-Parallel                 | HelpFile | Describes the ForEach -Parallel language construct in                                        |
| about_Foreach                          | HelpFile | Describes a language command you can use to traverse all the items in a                      |
| about_Format.ps1xml                    | HelpFile | The Format.ps1xml files in Windows PowerShell define the default display                     |
| about_Functions                        | HelpFile | Describes how to create and use functions in Windows PowerShell.                             |
| about_Functions_Advanced               | HelpFile | Introduces advanced functions that act similar to cmdlets.                                   |
| about_Functions_Advanced_Methods       | HelpFile | Describes how functions that specify the CmdletBinding attribute can use                     |
| about_Functions_Advanced_Parameters    | HelpFile | Explains how to add parameters to advanced functions.                                        |
| about_Functions_CmdletBindingAttribute | HelpFile | Describes the attribute that makes a function work like a                                    |
| about_Functions_OutputTypeAttribute    | HelpFile | Describes an attribute that reports the type of object that the function                     |
| about_Group_Policy_Settings            | HelpFile | Describes the Group Policy settings for Windows PowerShell                                   |
| about_Hash_Tables                      | HelpFile | Describes how to create, use, and sort hash tables in Windows PowerShell.                    |
| about_Hidden                           | HelpFile | Describes the Hidden keyword, which hides class members from default Get-Member results.     |
| about_History                          | HelpFile | Describes how to get and run commands in the command history.                                |
| about_If                               | HelpFile | Describes a language command you can use to run statement lists based                        |
| about_InlineScript                     | HelpFile | Describes the InlineScript activity, which runs Windows                                      |
| about_Jobs                             | HelpFile | Provides information about how Windows PowerShell background jobs run a                      |
| about_Job_Details                      | HelpFile | Provides details about background jobs on local and remote computers.                        |
| about_Join                             | HelpFile | Describes how the join operator (-join) combines multiple strings into a                     |
| about_Language_Keywords                | HelpFile | Describes the keywords in the Windows PowerShell scripting language.                         |
| about_Language_Modes                   | HelpFile | Explains language modes and their effect on Windows                                          |
| about_Line_Editing                     | HelpFile | Describes how to edit commands at the Windows PowerShell command prompt.                     |
| about_Locations                        | HelpFile | Describes how to access items from the working location in Windows                           |
| about_Logical_Operators                | HelpFile | Describes the operators that connect statements in Windows PowerShell.                       |
| about_Methods                          | HelpFile | Describes how to use methods to perform actions on objects in Windows                        |
| about_Modules                          | HelpFile | Explains how to install, import, and use Windows PowerShell modules.                         |
| about_Objects                          | HelpFile | Provides essential information about objects in Windows PowerShell.                          |
| about_Object_Creation                  | HelpFile | Explains how to create objects in Windows PowerShell.                                        |
| about_Operators                        | HelpFile | Describes the operators that are supported by Windows PowerShell.                            |
| about_Operator_Precedence              | HelpFile | Lists the Windows PowerShell operators in precedence order.                                  |
| about_PackageManagement                | HelpFile | PackageManagement is an aggregator for software package managers.                            |
| about_Parallel                         | HelpFile | Describes the Parallel keyword, which runs the                                               |
| about_Parameters                       | HelpFile | Describes how to work with command parameters in Windows PowerShell.                         |
| about_Parameters_Default_Values        | HelpFile | Describes how to set custom default values for the parameters of cmdlets and                 |
| about_Parsing                          | HelpFile | Describes how Windows PowerShell parses commands.                                            |
| about_Parsing_LocTest                  | HelpFile | Describes how Windows PowerShell parses commands.                                            |
| about_Path_Syntax                      | HelpFile | Describes the full and relative path name formats in Windows PowerShell.                     |
| about_Pipelines                        | HelpFile | Combining commands into pipelines in the Windows PowerShell                                  |
| about_PowerShell.exe                   | HelpFile | Explains how to use the PowerShell.exe command-line tool. Displays                           |
| about_PowerShell_Ise.exe               | HelpFile | Explains how to use the PowerShell_Ise.exe command-line tool.                                |
| about_Preference_Variables             | HelpFile | Variables that customize the behavior of Windows PowerShell                                  |
| about_Profiles                         | HelpFile | Describes how to create and use a Windows PowerShell profile.                                |
| about_Prompts                          | HelpFile | Describes the Prompt function and demonstrates how to create a custom                        |
| about_Properties                       | HelpFile | Describes how to use object properties in Windows PowerShell.                                |
| about_Providers                        | HelpFile | Describes how Windows PowerShell providers provide access to data and                        |
| about_PSConsoleHostReadLine            | HelpFile | Explains how to create a customize how Windows PowerShell reads input                        |
| about_PSReadline                       | HelpFile |                                                                                              |
| about_PSSessions                       | HelpFile | Describes Windows PowerShell sessions (PSSessions) and explains how to                       |
| about_PSSession_Details                | HelpFile | Provides detailed information about Windows PowerShell sessions and the                      |
| about_PSSnapins                        | HelpFile | Describes Windows PowerShell snap-ins and shows how to use and manage them.                  |
| about_Quoting_Rules                    | HelpFile | Describes rules for using single and double quotation marks                                  |
| about_Redirection                      | HelpFile | Explains how to redirect output from Windows PowerShell to text files.                       |
| about_Ref                              | HelpFile | Describes how to create and use a reference variable type.                                   |
| about_Regular_Expressions              | HelpFile | Describes regular expressions in Windows PowerShell.                                         |
| about_Remote                           | HelpFile | Describes how to run remote commands in Windows PowerShell.                                  |
| about_Remote_Disconnected_Sessions     | HelpFile | Explains how to disconnect from and reconnect to a PSSession                                 |
| about_Remote_FAQ                       | HelpFile | Contains questions and answers about running remote commands                                 |
| about_Remote_Jobs                      | HelpFile | Describes how to run background jobs on remote computers.                                    |
| about_Remote_Output                    | HelpFile | Describes how to interpret and format the output of remote commands.                         |
| about_Remote_Requirements              | HelpFile | Describes the system requirements and configuration requirements for                         |
| about_Remote_Troubleshooting           | HelpFile | Describes how to troubleshoot remote operations in Windows PowerShell.                       |
| about_Remote_Variables                 | HelpFile | Explains how to use local and remote variables in remote                                     |
| about_Requires                         | HelpFile | Prevents a script from running without the required elements.                                |
| about_Reserved_Words                   | HelpFile | Lists the reserved words that cannot be used as identifiers because they                     |
| about_Return                           | HelpFile | Exits the current scope, which can be a function, script, or script block.                   |
| about_Run_With_PowerShell              | HelpFile | Explains how to use the "Run with PowerShell" feature to run                                 |
| about_Scheduled_Jobs                   | HelpFile | Describes scheduled jobs and explains how to use and manage                                  |
| about_Scheduled_Jobs_Advanced          | HelpFile | Explains advanced scheduled job topics, including the file structure                         |
| about_Scheduled_Jobs_Basics            | HelpFile | Explains how to create and manage scheduled jobs.                                            |
| about_Scheduled_Jobs_Troubleshooting   | HelpFile | Explains how to resolve problems with scheduled jobs                                         |
| about_Scopes                           | HelpFile | Explains the concept of scope in Windows PowerShell and shows how to set                     |
| about_Scripts                          | HelpFile | Describes how to run and write scripts in Windows PowerShell.                                |
| about_Script_Blocks                    | HelpFile | Defines what a script block is and explains how to use script blocks in                      |
| about_Script_Internationalization      | HelpFile | Describes the script internationalization features of Windows PowerShell 2.0                 |
| about_Sequence                         | HelpFile | Describes the Sequence keyword, which runs selected                                          |
| about_Session_Configurations           | HelpFile | Describes session configurations, which determine the users who can                          |
| about_Session_Configuration_Files      | HelpFile | Describes session configuration files, which can be used in a                                |
| about_Signing                          | HelpFile | Explains how to sign scripts so that they comply with the Windows                            |
| about_Special_Characters               | HelpFile | Describes the special characters that you can use to control how                             |
| about_Splatting                        | HelpFile | Describes how to use splatting to pass parameters to commands                                |
| about_Split                            | HelpFile | Explains how to use the Split operator to split one or more strings into                     |
| about_Suspend-Workflow                 | HelpFile | Describes the Suspend-Workflow activity, which suspends                                      |
| about_Switch                           | HelpFile | Explains how to use a switch to handle multiple If statements.                               |
| about_Throw                            | HelpFile | Describes the Throw keyword, which generates a terminating error.                            |
| about_Transactions                     | HelpFile | Describes how to manage transacted operations in Windows PowerShell.                         |
| about_Trap                             | HelpFile | Describes a keyword that handles a terminating error.                                        |
| about_Try_Catch_Finally                | HelpFile | Describes how to use the Try, Catch, and Finally blocks to handle                            |
| about_Types.ps1xml                     | HelpFile | Explains how to use Types.ps1xml files to extend the types of objects                        |
| about_Type_Operators                   | HelpFile | Describes the operators that work with Microsoft .NET Framework types.                       |
| about_Updatable_Help                   | HelpFile | Describes the updatable help system in Windows PowerShell.                                   |
| about_Variables                        | HelpFile | Describes how variables store values that can be used in Windows                             |
| about_While                            | HelpFile | Describes a language statement that you can use to run a command block                       |
| about_Wildcards                        | HelpFile | Describes how to use wildcard characters in Windows PowerShell.                              |
| about_Windows_PowerShell_5.0           | HelpFile | Describes new features that are included in                                                  |
| about_Windows_PowerShell_ISE           | HelpFile | Describes the features and system requirements of Windows PowerShell                         |
| about_Windows_RT                       | HelpFile | Explains limitations of Windows PowerShell 4.0 on Windows RT 8.1.                            |
| about_WMI                              | HelpFile | Windows Management Instrumentation (WMI) uses the                                            |
| about_WMI_Cmdlets                      | HelpFile | Provides background information about Windows Management Instrumentation                     |
| about_WorkflowCommonParameters         | HelpFile | This topic describes the parameters that are valid on all Windows                            |
| about_Workflows                        | HelpFile | Provides a brief introduction to the Windows                                                 |
| about_WQL                              | HelpFile | Describes WMI Query Language (WQL), which can be                                             |
| about_WS-Management_Cmdlets            | HelpFile | Provides an overview of Web Services for Management (WS-Management) as                       |
| about_BeforeEach_AfterEach             | HelpFile | performed at the beginning and end of every It block. This can eliminate duplication of code |
| about_Mocking                          | HelpFile | Pester provides a set of Mocking functions making it easy to fake dependencies               |
| about_Pester                           | HelpFile | Pester is a BDD based test runner for PowerShell.                                            |
| about_should                           | HelpFile | Provides assertion convenience methods for comparing objects and throwing                    |
| about_TestDrive                        | HelpFile | A PSDrive for file activity limited to the scope of a singe Describe or                      |
| about_Scheduled_Jobs                   | HelpFile | Describes scheduled jobs and explains how to use and manage                                  |
| about_Scheduled_Jobs_Advanced          | HelpFile | Explains advanced scheduled job topics, including the file structure                         |
| about_Scheduled_Jobs_Basics            | HelpFile | Explains how to create and manage scheduled jobs.                                            |
| about_Scheduled_Jobs_Troubleshooting   | HelpFile | Explains how to resolve problems with scheduled jobs                                         |
| about_ActivityCommonParameters         | HelpFile | Describes the parameters that Windows PowerShell                                             |
| about_Checkpoint-Workflow              | HelpFile | Describes the Checkpoint-Workflow activity, which                                            |
| about_ForEach-Parallel                 | HelpFile | Describes the ForEach -Parallel language construct in                                        |
| about_InlineScript                     | HelpFile | Describes the InlineScript activity, which runs Windows                                      |
| about_Parallel                         | HelpFile | Describes the Parallel keyword, which runs the                                               |
| about_Sequence                         | HelpFile | Describes the Sequence keyword, which runs selected                                          |
| about_Suspend-Workflow                 | HelpFile | Describes the Suspend-Workflow activity, which suspends                                      |
| about_WorkflowCommonParameters         | HelpFile | This topic describes the parameters that are valid on all Windows                            |
| about_Workflows                        | HelpFile | Provides a brief introduction to the Windows                                                 |
