# 正则表达式

## 字符组

| 字符组 | 具体含义                                                     |
| ------ | ------------------------------------------------------------ |
| `\d`   | 表示 `[0-9]`。表示是一位数字。 记忆方式：其英文是 digit（数字）。 |
| `\D`   | 表示 `[^0-9]`。表示除数字外的任意字符。                      |
| `\w`   | 表示 `[0-9a-zA-Z_]`。表示数字、大小写字母和下划线。记忆方式：w 是 word 的简写，也称单词字符。 |
| `\W`   | 表示 `[^0-9a-zA-Z_]`。非单词字符。                           |
| `\s`   | 表示 `[ \t\v\n\r\f]`。表示空白符，包括空格、水平制表符、垂直制表符、换行符、回车符、换页符。记忆方式：s 是 space 的首字母，空白符的单词是 white space。 |
| `\S`   | 表示 `[^ \t\v\n\r\f]`。 非空白符。                           |
| `.`    | 表示 `[^\n\r\u2028\u2029]`。通配符，表示几乎任意字符。换行符、回车符、行分隔符和段分隔符除外。记忆方式：想想省略号 … 中的每个点，都可以理解成占位符，表示任何类似的东西。 |



## 1. 贪婪与惰性模式匹配

### 1.1. 贪婪匹配

```js
var regex = /\d{2,5}/g;
var string = "123 1234 12345 123456";
console.log( string.match(regex) );
// => ["123", "1234", "12345", "12345"]
/*
其中正则 /\d{2,5}/，表示数字连续出现 2 到 5 次。会匹配 2 位、3 位、4 位、5 位连续数字。
但是其是贪婪的，它会尽可能多的匹配。你能给我 6 个，我就要 5 个。你能给我 3 个，我就要 3 个。
反正只要在能力范围内，越多越好。
*/
```

### 1.2. 非贪婪匹配-惰性匹配

```js
var regex = /\d{2,5}?/g;
var string = "123 1234 12345 123456";
console.log( string.match(regex) );
// => ["12", "12", "34", "12", "34", "12", "34", "56"]
/*
其中 /\d{2,5}?/ 表示，虽然 2 到 5 次都行，当 2 个就够的时候，就不再往下尝试了。
*/
```

### 1.3. 量词贪婪总结 

| 惰性量词      | 贪婪量词 |
| ------------- | ---- |
| {m,n}?        | {m,n} |
| {m,}?         | {m,} |
| ?? | ? |
| +? | + |
| *? | * |

[^注意]: 对惰性匹配的记忆方式是：量词后面加个问号，问一问你知足了吗，你很贪婪吗？

### 1.4.  分支结构--默认惰性匹配

```js
var regex = /good|goodbye/g;
var string = "goodbye";
console.log( string.match(regex) );
// => ["good"]

var regex = /goodbye|good/g;
var string = "goodbye";
console.log( string.match(regex) );
// => ["goodbye"]
```

[^注意]: 也就是说，分支结构也是惰性的，即当前面的匹配上了，后面的就不再尝试了。

## 2. 分组结果操作

### 2.1. 提取

```js
//也可以使用构造函数的全局属性 $1 至 $9 来获取：
var regex = /(\d{4})-(\d{2})-(\d{2})/;
var string = "2017-06-12";
regex.test(string); // 正则操作即可，例如
//regex.exec(string);
//string.match(regex);
console.log(RegExp.$1); // "2017"
console.log(RegExp.$2); // "06"
console.log(RegExp.$3); // "12"
```

### 2.2. 替换

```js
var regex = /(\d{4})-(\d{2})-(\d{2})/;
var string = "2017-06-12";
var result = string.replace(regex, "$2/$3/$1");
console.log(result);

//等价
var result = string.replace(regex, function () {
return RegExp.$2 + "/" + RegExp.$3 + "/" + RegExp.$1;
});

var result = string.replace(regex, function (match, year, month, day) {
return month + "/" + day + "/" + year;
});
```

### 2.3. 括号嵌套处理分组

```js
//以左括号（开括号）为准, 左括号--从左往右进行组别区分
var regex = /^((\d)(\d(\d)))\1\2\3\4$/;
var string = "1231231233";
console.log( regex.test(string) ); // true
console.log( RegExp.$1 ); // 123
console.log( RegExp.$2 ); // 1
console.log( RegExp.$3 ); // 23
console.log( RegExp.$4 ); // 3
```

