# 常用表达式

## 货币格式显示

```js
function format (num) {
return num.toFixed(2).replace(/\B(?=(\d{3})+\b)/g, ",").replace(/^/, "$$ ");
};
console.log( format(1888) );
// => "$ 1,888.00"
```

## 匹配 16 进制颜色值

```js
var regex = /#([0-9a-fA-F]{6}|[0-9a-fA-F]{3})/g;
var string = "#ffbbad #Fc01DF #FFF #ffE";
console.log( string.match(regex) );
/*
数据源
#ffbbad
#Fc01DF
#FFF
#ffE
*/
// => ["#ffbbad", "#Fc01DF", "#FFF", "#ffE"]
```

## 匹配时间

```js
/*
分析：
共 4 位数字，第一位数字可以为 [0-2]。
当第 1 位为 "2" 时，第 2 位可以为 [0-3]，其他情况时，第 2 位为 [0-9]。
第 3 位数字为 [0-5]，第4位为 [0-9]。

23:59
02:07
*/
//保留0占位符
var regex = /^([01][0-9]|[2][0-3]):[0-5][0-9]$/;
console.log( regex.test("23:59") );
console.log( regex.test("02:07") );
// => true
// => true

//取消0占位符
var regex = /^(0?[0-9]|1[0-9]|[2][0-3]):(0?[0-9]|[1-5][0-9])$/;
console.log( regex.test("23:59") );
console.log( regex.test("02:07") );
console.log( regex.test("7:9") );
// => true
// => true
// => true
```

## 匹配日期

```js
/*
分析：
年，四位数字即可，可用 [0-9]{4}。
月，共 12 个月，分两种情况 "01"、"02"、…、"09" 和 "10"、"11"、"12"，可用 (0[1-9]|1[0-2])。
日，最大 31 天，可用 (0[1-9]|[12][0-9]|3[01])。

匹配数据源
2017-06-10
*/

var regex = /^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])$/;
console.log( regex.test("2017-06-10") );
// => true
```

## ✨window 操作系统文件路径

```js
/*
这个例子比较难理解，后续进行理解操作
数据源：
F:\study\javascript\regex\regular expression.pdf
F:\study\javascript\regex\
F:\study\javascript
F:\
*/
var regex = /^[a-zA-Z]:\\([^\\:*<>|"?\r\n/]+\\)*([^\\:*<>|"?\r\n/]+)?$/;
console.log( regex.test("F:\\study\\javascript\\regex\\regular expression.pdf") );
console.log( regex.test("F:\\study\\javascript\\regex\\") );
console.log( regex.test("F:\\study\\javascript") );
console.log( regex.test("F:\\") );
// => true
// => true
// => true
// => true
```

## ✨验证密码问题--直接参考文档里的节点

## 匹配日期格式-分隔符

```js
/*
2016-06-12
2016/06/12
2016.06.12
*/

//常规
var regex = /\d{4}(-|\/|\.)\d{2}(-|\/|\.)\d{2}/;

//使用反向引用， [\1]代表着第一个分组
var regex = /\d{4}(-|\/|\.)\d{2}\1\d{2}/;
```

## 实现Trim方法

```js
//替换前边空格和尾部空格即可
function trim(str) {
return str.replace(/^\s+|\s+$/g, '');
}
console.log( trim(" foobar ") );
```

## 将每个单词的首字母转换为大写

```js
function titleize (str) {
return str.toLowerCase().replace(/(?:^|\s)\w/g, function (c) {
return c.toUpperCase();
});
}
console.log( titleize('my name is epeli') );
// => "My Name Is Epeli"
```

